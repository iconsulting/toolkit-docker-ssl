# WebGateway for Docker Services

This solution aims to be a TLS termination for all services exposed by a single machine.

Typical issues in this scenario are:

- unable to standard port for serveral services: each docker service is exposed to a different port
- setting https certificates is a mess

With this solution

- any docker service can be exposed via https
- you can configure TLS terminantion once, and you can force HTTPS redirections.

## HOW

With this solution only traefik reverse proxy exposes default http ports (80 http, 443 https) 
and it dispatch requests to the right container based on dns name.

Each port exposed by containers is not visibile from the host machine, it is visibile only from the compose network.

> Each docker compose has its own isolated network

```mermaid
graph LR

  client -- 80 --> web
  client -- 443 --> websecure
  web --> websecure

  subgraph docker
  
  subgraph traefik
    web
    websecure
  end

  websecure -- 8080 --- service-a
  websecure -- 8080 --- service-b
  websecure -- 9000 --- service-c

  end


```

# Exposing ports

If your solution requires multiple docker compose, yo need to create an external network and make sure that all services are connected to that network.

> Use plain docker insted of docker compose is not reccomended becouse services will be expsed by host machine

Since that each docker-compose create a dedicated network, service ports need to be exposed in some way.

Usually ports are exposed directly to host via:
``` yml
version: "3.4"

services:
  servicename: 
    ports: 
      - "8080:4200"  # host:container ports
```

With this configuration, container ports need to be exposed to a dedicated docker network `webgateway` where traefik is listen to. Then traefik will be exposed to host with standard ports.

``` yml
version: "3.4"

services:
  servicename:
    networks:
      - default # default compose network
      - web     # variable name to network config below

networks:
  web:
    external:
      name: webgateway # real docker network name
```

# Træfik

The webgateway choosed for contenerizated apps is Træfik, a modern reverse proxy and load balancer with almost zero conf.

Please refer to official documentation to perform additional configurations:

for local development the following tools can be useful:
- [mkcert](https://github.com/FiloSottile/mkcert)
- [localtest.me](readme.localtest.me)

# Bonus

This method allow to scale docker services with round robin logic:

```
> docker compose up -d --scale echo=3
```
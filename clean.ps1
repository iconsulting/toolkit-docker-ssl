
# Removes dangling images
docker rmi $(docker images -f "dangling=true" -q)

# Remove dangling volumes
docker volume rm $(docker volume ls -qf dangling=true)

# Removes exited container
docker rm $(docker ps -aq)

# Seems useful resources:
# https://gist.github.com/bastman/5b57ddb3c11942094f8d0a97d461b430
# https://gist.github.com/johnpapa/0bd3df2a21b3a448b97ef3f429bd7145